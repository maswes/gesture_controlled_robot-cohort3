# README #

This README documents the steps necessary to get this application up and running.

### What is this repository for? ###

####This project provides a hardware and software interface to remotely control a Rumba's i-Robot Create using hand gestures with the Thalmics lab’s Myo armband.
The SW interface is developed on LabVIEW and Python.
The HW interface between the Thalmics lab’s Myo armband and the i-Robot has been implemented using a NI MyRIO.

* Version 1.0

### How do I get set up? ###

####0. Configure the NI MyRIO TCP address
[How to configure NI MyRIO TCP address](https://bitbucket.org/liejf/capstone/wiki/How%20to%20configure%20NIMyRio%20to%20work%20with%20your%20WiFi)

####1. Install python 2.7, and all the python modules listed at the [Wiki ](https://bitbucket.org/liejf/capstone/wiki/Home)page of this project; and copy all the python scripts located under the "/ MyRio files / natinst / myo-raw-master /" directory of this repository,  into the NI MyRIO at "/ natinst / myo-raw-master /". 

You can connect to the NI MyRIO using SSH with [Tera Term](http://ttssh2.osdn.jp/index.html.en), or with your preferred terminal emulator, to change the access permission to the several sub-directories in MyRIO's RT Linux using 
```
#!ssh

CHMOD 999
```
Then, you can copy/paste the files located under the /MyRIO folder in this repository, into the corresponding sub-directories in the MyRIO RT-Linux using WebDAV to transfer files between your host computer and NI MyRIO. 

* For a list of useful SSH commands see:[Useful SSH Commands for configuring Linux RT on myRIO or cRIO-9068](https://decibel.ni.com/content/docs/DOC-37109)

* For details about how Install python packages into NI MyRIO using opkg, check: [Step-by-step: Install/Configure a Package using opkg on Linux RTOS (myRIO and cRIO-9068)](https://decibel.ni.com/content/docs/DOC-36980)

* You can find some of the required python packages at: [http://download.ni.com/ni-linux-rt/feeds/2014/arm/](http://download.ni.com/ni-linux-rt/feeds/2014/arm/)

* For detail about how to configure WebDAV, check: [Using WebDAV to Transfer files into your RT target](http://digital.ni.com/public.nsf/websearch/4EBE45E8A816B19386257B6C0071D025?OpenDocument) 

####2. Install LabVIEW myRIO 2013 (More recent versions of LabVIEW should work too)
####3. Download the LabVIEW project and VIs located at "/Labview/Myo Control" into your Windows host computer
####4. Update the NI MyRIO TCP address in the Labview's Myo Control project
####5. At this point you should have all the software and files required to control the iRobot with the Myo armband


*** Controlling the Robot with default poses with VIs running exclusively on the RT target**


1. Open the 'Myo Control.lvproj' in LabVIEW
2. Run 'myo control gestures.vi' (This will deploy all the necessary files into the RT target)
3. Do the Sync gesture (Refer to Thalmics lab documentation)
4. Now proceed to control the Robot using the following gestures

* Fist - Forward
* Fingers spread - Backwards
* Wave in - Turn Left
* Wave out - Turn Right
* Double tap thumb to middle finger - Stop

*** Controlling the Robot with default and additional poses with a combination of VIs running on the RT target and the Host**


1. Open the 'Myo Control.lvproj' in LabVIEW
2. Launch 'Myo raw MLTV3 host.vi' on the host (Open it and click on the Run VI button)
3. Run 'myo raw MLTV3_RT ROBOT.vi' on the RT target (This will deploy all the necessary files into the RT target)
4. Do the Sync gesture (Refer to Thalmics lab documentation)
4. Now proceed to control the Robot using the following gestures

* Fist - Forward
* Fingers spread - Backwards
* Wave in - Turn Left
* Wave out - Turn Right
* Double tap thumb to middle finger - Stop

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact