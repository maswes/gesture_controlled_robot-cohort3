﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="13008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="varPersistentID:{4371B43C-9950-4917-9B47-E3E38E3B88FF}" Type="Ref">/My Computer/Host/Host Target Communication.lvlib/Pose_raw_data</Property>
	<Property Name="varPersistentID:{5435896E-3170-47C1-8DBC-E6CE061D3CC3}" Type="Ref">/My Computer/Host/Host Target Communication.lvlib/Pose_String</Property>
	<Property Name="varPersistentID:{AD10C495-530B-4246-8288-F0AB8E29D6E5}" Type="Ref">/Ni MyRIO/RT/RT variables.lvlib/Stop RT</Property>
	<Property Name="varPersistentID:{D0D9B505-2C43-44F8-84CF-D2428395BFCC}" Type="Ref">/My Computer/Host/Host Target Communication.lvlib/Stop</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Host" Type="Folder">
			<Item Name="Host Target Communication.lvlib" Type="Library" URL="../Host/Host Target Communication.lvlib"/>
			<Item Name="MLT_KNN_V2.vi" Type="VI" URL="../RT/MLT_KNN_V2.vi"/>
			<Item Name="myo raw MLTV3 host.vi" Type="VI" URL="../RT/myo raw MLTV3 host.vi"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Internecine Avoider.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/Internecine Avoider.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="NILabs_MLT.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/Machine Learning/NILabs_MLT.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Read From Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (DBL).vi"/>
				<Item Name="Read From Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (I64).vi"/>
				<Item Name="Read From Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (string).vi"/>
				<Item Name="Read From Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File.vi"/>
				<Item Name="Read Lines From File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="TCP Listen Internal List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen Internal List.vi"/>
				<Item Name="TCP Listen List Operations.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen List Operations.ctl"/>
				<Item Name="TCP Listen.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="InitTrainingDataV2.vi" Type="VI" URL="../RT/InitTrainingDataV2.vi"/>
			<Item Name="launch myo raw client .vi" Type="VI" URL="../RT/launch myo raw client .vi"/>
			<Item Name="Read and index array.vi" Type="VI" URL="../RT/Read and index array.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="Ni MyRIO" Type="RT myRIO">
		<Property Name="alias.name" Type="Str">Ni MyRIO</Property>
		<Property Name="alias.value" Type="Str">192.168.1.90</Property>
		<Property Name="CCSymbols" Type="Str">OS,Linux;CPU,ARM;DeviceCode,762F;TARGET_TYPE,RT;</Property>
		<Property Name="crio.ControllerPID" Type="Str">762F</Property>
		<Property Name="crio.family" Type="Str">ARMLinux</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">8</Property>
		<Property Name="host.TargetOSID" Type="UInt">8</Property>
		<Property Name="mathScriptPath" Type="Str">C:\Users\alie\Documents\LabVIEW Data</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Enabled" Type="Bool">true</Property>
		<Property Name="target.IOScan.Faults" Type="Str">1.0,0;</Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">10000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTDebugWebServerHTTPPort" Type="UInt">8001</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/c/ni-rt/startup/startup.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">false</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/c/ni-rt/startup</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str"></Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.viscripting.showScriptingOperationsInContextHelp" Type="Bool">false</Property>
		<Property Name="target.server.viscripting.showScriptingOperationsInEditor" Type="Bool">false</Property>
		<Property Name="target.WebServer.Config" Type="Str"># Web server configuration file.
# Generated by LabVIEW 13.0.1
# 3/7/2015 9:26:22 AM

#
# Global Directives
#
NI.AddLVRouteVars
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
LimitWorkers 10
LoadModulePath "$LVSERVER_MODULEPATHS"
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule
Listen 8000

#
# Directives that apply to the default server
#
NI.ServerName default
DocumentRoot "$LVSERVER_DOCROOT"
InactivityTimeout 60
SetConnector netConnector
AddHandler LVAuth
AddHandler LVRFP
AddHandler fileHandler ""
AddOutputFilter chunkFilter
DirectoryIndex index.htm
</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Item Name="RT" Type="Folder">
			<Item Name="iRobot VIs" Type="Folder">
				<Item Name="Actuator" Type="Folder">
					<Item Name="Digital Outputs.vi" Type="VI" URL="../RT/iRobot VIs/Actuator/Digital Outputs.vi"/>
					<Item Name="Drive Direct.vi" Type="VI" URL="../RT/iRobot VIs/Actuator/Drive Direct.vi"/>
					<Item Name="Drive Direction.ctl" Type="VI" URL="../RT/iRobot VIs/Actuator/Drive Direction.ctl"/>
					<Item Name="Drive Direction.vi" Type="VI" URL="../RT/iRobot VIs/Actuator/Drive Direction.vi"/>
					<Item Name="Drive Radius.vi" Type="VI" URL="../RT/iRobot VIs/Actuator/Drive Radius.vi"/>
					<Item Name="LED.ctl" Type="VI" URL="../RT/iRobot VIs/Actuator/LED.ctl"/>
					<Item Name="LED.vi" Type="VI" URL="../RT/iRobot VIs/Actuator/LED.vi"/>
					<Item Name="PWM Low-Side Driver.vi" Type="VI" URL="../RT/iRobot VIs/Actuator/PWM Low-Side Driver.vi"/>
					<Item Name="Song Define.vi" Type="VI" URL="../RT/iRobot VIs/Actuator/Song Define.vi"/>
					<Item Name="Song Note.ctl" Type="VI" URL="../RT/iRobot VIs/Actuator/Song Note.ctl"/>
					<Item Name="Song Play.vi" Type="VI" URL="../RT/iRobot VIs/Actuator/Song Play.vi"/>
					<Item Name="Stop.vi" Type="VI" URL="../RT/iRobot VIs/Actuator/Stop.vi"/>
				</Item>
				<Item Name="Advanced" Type="Folder">
					<Item Name="Command" Type="Folder">
						<Item Name="Baud.ctl" Type="VI" URL="../RT/iRobot VIs/Advanced/Command/Baud.ctl"/>
						<Item Name="Baud.vi" Type="VI" URL="../RT/iRobot VIs/Advanced/Command/Baud.vi"/>
						<Item Name="Demo Numbers.ctl" Type="VI" URL="../RT/iRobot VIs/Advanced/Command/Demo Numbers.ctl"/>
						<Item Name="Demo Stop.vi" Type="VI" URL="../RT/iRobot VIs/Advanced/Command/Demo Stop.vi"/>
						<Item Name="Demo.vi" Type="VI" URL="../RT/iRobot VIs/Advanced/Command/Demo.vi"/>
						<Item Name="Full.vi" Type="VI" URL="../RT/iRobot VIs/Advanced/Command/Full.vi"/>
						<Item Name="OpCode.ctl" Type="VI" URL="../RT/iRobot VIs/Advanced/Command/OpCode.ctl"/>
						<Item Name="Safe.vi" Type="VI" URL="../RT/iRobot VIs/Advanced/Command/Safe.vi"/>
						<Item Name="Start.vi" Type="VI" URL="../RT/iRobot VIs/Advanced/Command/Start.vi"/>
					</Item>
					<Item Name="UART" Type="Folder">
						<Item Name="iRobot UART Byte Array Checksum.vi" Type="VI" URL="../RT/iRobot VIs/Advanced/UART/iRobot UART Byte Array Checksum.vi"/>
						<Item Name="iRobot UART Bytes at Port.vi" Type="VI" URL="../RT/iRobot VIs/Advanced/UART/iRobot UART Bytes at Port.vi"/>
						<Item Name="iRobot UART Close.vi" Type="VI" URL="../RT/iRobot VIs/Advanced/UART/iRobot UART Close.vi"/>
						<Item Name="iRobot UART Open.vi" Type="VI" URL="../RT/iRobot VIs/Advanced/UART/iRobot UART Open.vi"/>
						<Item Name="iRobot UART Port Type.ctl" Type="VI" URL="../RT/iRobot VIs/Advanced/UART/iRobot UART Port Type.ctl"/>
						<Item Name="iRobot UART Read Flush.vi" Type="VI" URL="../RT/iRobot VIs/Advanced/UART/iRobot UART Read Flush.vi"/>
						<Item Name="iRobot UART Read Simple Packet.vi" Type="VI" URL="../RT/iRobot VIs/Advanced/UART/iRobot UART Read Simple Packet.vi"/>
						<Item Name="iRobot UART Read.vi" Type="VI" URL="../RT/iRobot VIs/Advanced/UART/iRobot UART Read.vi"/>
						<Item Name="iRobot UART Simple Packet Configuration.ctl" Type="VI" URL="../RT/iRobot VIs/Advanced/UART/iRobot UART Simple Packet Configuration.ctl"/>
						<Item Name="iRobot UART Write.vi" Type="VI" URL="../RT/iRobot VIs/Advanced/UART/iRobot UART Write.vi"/>
					</Item>
				</Item>
				<Item Name="Sensor" Type="Folder">
					<Item Name="Advanced" Type="Folder">
						<Item Name="Parse 8.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Advanced/Parse 8.vi"/>
						<Item Name="Parse 16.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Advanced/Parse 16.vi"/>
						<Item Name="Parse Boolean.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Advanced/Parse Boolean.vi"/>
						<Item Name="Parse Bumps + Wheel Drops.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Advanced/Parse Bumps + Wheel Drops.vi"/>
						<Item Name="Parse Buttons.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Advanced/Parse Buttons.vi"/>
						<Item Name="Parse Cargo Bay Digital Inputs.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Advanced/Parse Cargo Bay Digital Inputs.vi"/>
						<Item Name="Parse Charging Sources Available.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Advanced/Parse Charging Sources Available.vi"/>
						<Item Name="Parse Low-Side Driver + Wheel Overcurrents.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Advanced/Parse Low-Side Driver + Wheel Overcurrents.vi"/>
						<Item Name="Parse Sensor Group 0.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Advanced/Parse Sensor Group 0.vi"/>
						<Item Name="Parse Sensor Group 1.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Advanced/Parse Sensor Group 1.vi"/>
						<Item Name="Parse Sensor Group 2.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Advanced/Parse Sensor Group 2.vi"/>
						<Item Name="Parse Sensor Group 3.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Advanced/Parse Sensor Group 3.vi"/>
						<Item Name="Parse Sensor Group 4.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Advanced/Parse Sensor Group 4.vi"/>
						<Item Name="Parse Sensor Group 5.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Advanced/Parse Sensor Group 5.vi"/>
						<Item Name="Parse Sensor Group 6.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Advanced/Parse Sensor Group 6.vi"/>
					</Item>
					<Item Name="Polling" Type="Folder">
						<Item Name="Poll All Sensors.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Polling/Poll All Sensors.vi"/>
						<Item Name="Poll Sensor Group 0.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Polling/Poll Sensor Group 0.vi"/>
						<Item Name="Poll Sensor Group 1.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Polling/Poll Sensor Group 1.vi"/>
						<Item Name="Poll Sensor Group 2.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Polling/Poll Sensor Group 2.vi"/>
						<Item Name="Poll Sensor Group 3.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Polling/Poll Sensor Group 3.vi"/>
						<Item Name="Poll Sensor Group 4.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Polling/Poll Sensor Group 4.vi"/>
						<Item Name="Poll Sensor Group 5.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Polling/Poll Sensor Group 5.vi"/>
						<Item Name="Poll Sensor Group 6.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Polling/Poll Sensor Group 6.vi"/>
						<Item Name="Poll Sensor.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Polling/Poll Sensor.vi"/>
					</Item>
					<Item Name="Streaming" Type="Folder">
						<Item Name="Advanced" Type="Folder">
							<Item Name="Stream Simulate.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Streaming/Advanced/Stream Simulate.vi"/>
						</Item>
						<Item Name="Stream Begin All Sensors.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Streaming/Stream Begin All Sensors.vi"/>
						<Item Name="Stream Configure.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Streaming/Stream Configure.vi"/>
						<Item Name="Stream Pause.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Streaming/Stream Pause.vi"/>
						<Item Name="Stream Update All Sensors.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Streaming/Stream Update All Sensors.vi"/>
					</Item>
					<Item Name="Accumulate Distance and Angle.vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Accumulate Distance and Angle.vi"/>
					<Item Name="Bumps + Wheel Drops.ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/Bumps + Wheel Drops.ctl"/>
					<Item Name="Buttons.ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/Buttons.ctl"/>
					<Item Name="Cargo Bay Digital Inputs.ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/Cargo Bay Digital Inputs.ctl"/>
					<Item Name="Charging Sources Available.ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/Charging Sources Available.ctl"/>
					<Item Name="Charging State.ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/Charging State.ctl"/>
					<Item Name="Low-Side Driver + Wheel Overcurrent.ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/Low-Side Driver + Wheel Overcurrent.ctl"/>
					<Item Name="OI Mode.ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/OI Mode.ctl"/>
					<Item Name="Sensor Group 0.ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/Sensor Group 0.ctl"/>
					<Item Name="Sensor Group 1.ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/Sensor Group 1.ctl"/>
					<Item Name="Sensor Group 2.ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/Sensor Group 2.ctl"/>
					<Item Name="Sensor Group 3.ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/Sensor Group 3.ctl"/>
					<Item Name="Sensor Group 4.ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/Sensor Group 4.ctl"/>
					<Item Name="Sensor Group 5.ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/Sensor Group 5.ctl"/>
					<Item Name="Sensor Group 6 (graphical).ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/Sensor Group 6 (graphical).ctl"/>
					<Item Name="Sensor Group 6 to Sensor Group 6 (graphical).vi" Type="VI" URL="../RT/iRobot VIs/Sensor/Sensor Group 6 to Sensor Group 6 (graphical).vi"/>
					<Item Name="Sensor Group 6.ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/Sensor Group 6.ctl"/>
					<Item Name="Sensor ID.ctl" Type="VI" URL="../RT/iRobot VIs/Sensor/Sensor ID.ctl"/>
				</Item>
				<Item Name="iRobot Close.vi" Type="VI" URL="../RT/iRobot VIs/iRobot Close.vi"/>
				<Item Name="iRobot Create Readme.pdf" Type="Document" URL="../RT/iRobot VIs/iRobot Create Readme.pdf"/>
				<Item Name="iRobot Open.vi" Type="VI" URL="../RT/iRobot VIs/iRobot Open.vi"/>
			</Item>
			<Item Name="InitTrainingDataV2.vi" Type="VI" URL="../RT/InitTrainingDataV2.vi"/>
			<Item Name="launch myo client.vi" Type="VI" URL="../RT/launch myo client.vi"/>
			<Item Name="myo control gestures.vi" Type="VI" URL="../RT/myo control gestures.vi"/>
			<Item Name="myo raw MLTV3_RT ROBOT.vi" Type="VI" URL="../RT/myo raw MLTV3_RT ROBOT.vi"/>
			<Item Name="myo raw MLTV3_RT.vi" Type="VI" URL="../RT/myo raw MLTV3_RT.vi"/>
			<Item Name="myo raw read save 2.vi" Type="VI" URL="../RT/myo raw read save 2.vi"/>
			<Item Name="Read and index array.vi" Type="VI" URL="../RT/Read and index array.vi"/>
			<Item Name="RT variables.lvlib" Type="Library" URL="../RT/RT variables.lvlib"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Internecine Avoider.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/Internecine Avoider.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="NILabs_MLT.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/Machine Learning/NILabs_MLT.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Read From Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (DBL).vi"/>
				<Item Name="Read From Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (I64).vi"/>
				<Item Name="Read From Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (string).vi"/>
				<Item Name="Read From Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File.vi"/>
				<Item Name="Read Lines From File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="TCP Listen Internal List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen Internal List.vi"/>
				<Item Name="TCP Listen List Operations.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen List Operations.ctl"/>
				<Item Name="TCP Listen.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write To Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (DBL).vi"/>
				<Item Name="Write To Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (I64).vi"/>
				<Item Name="Write To Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (string).vi"/>
				<Item Name="Write To Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File.vi"/>
			</Item>
			<Item Name="launch myo raw client .vi" Type="VI" URL="../RT/launch myo raw client .vi"/>
			<Item Name="MLT_KNN_V2.vi" Type="VI" URL="../RT/MLT_KNN_V2.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
