#!/usr/bin/python           # This is client_socket.py file

import socket               # Import socket module

class mysocket:
    '''demonstration class only
      - coded for clarity, not efficiency
    '''

    def __init__(self, sock=None):
        if sock is None:
            self.sock = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
        else:
            self.sock = sock

    def connect(self, host, port):
        self.sock.connect((host, port))

    def mysend(self, msg):
        totalsent = 0
        while totalsent < MSGLEN:
            sent = self.sock.send(msg[totalsent:])
            if sent == 0:
                raise RuntimeError("socket connection broken")
            totalsent = totalsent + sent

    def myreceive(self):
        chunks = []
        bytes_recd = 0
        while bytes_recd < MSGLEN:
            chunk = self.sock.recv(min(MSGLEN - bytes_recd, 2048))
            if chunk == '':
                raise RuntimeError("socket connection broken")
            chunks.append(chunk)
            bytes_recd = bytes_recd + len(chunk)
        return ''.join(chunks)
		
#create an INET, STREAMing socket for Rawdata at port 1025
serversocket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print ('server socket1')
#bind the socket to the local host,
# and  port 1025
serversocket1.bind(('localhost', 1025))
#become a server socket
serversocket1.listen(5)
#accept connections bv
(clientsocket1, address1) = serversocket1.accept()
print ('client socket1 accept')
#create an INET, STREAMing socket for Pose at port 1026
#serversocket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#print ('server socket2')
#bind the socket to the local host,
# and port 1026
#serversocket2.bind(('localhost', 1026))
#become a server socket
#serversocket2.listen(5)
#clientsocket2.mysend(p)
#accept connections 
#(clientsocket2, address2) = serversocket2.accept()
#now do something with the clientsocket
#in this case, we'll pretend this is a threaded server
#ct1 = client_thread(clientsocket1)
#ct2 = client_thread(clientsocket2)
#ct1.run()
#ct2.run()
#clientsocket2.mysend('Hello world!')

try:
	while True:
		pose = clientsocket1.recv(1024)
		if pose: print('server:',pose)
		#data = clientsocket2.recv(1024)
		#if data: print('server:',data) 
except KeyboardInterrupt:
	pass
finally:
	clientsocket1.close
	clientsocket2.close                     # Close the socket when done
	