#!/usr/bin/python           # This is client_socket.py file

import socket               # Import socket module

s = socket.socket()         # Create a socket object
host = socket.gethostname() # Get local machine name
port = 1026                 # Reserve a port for your service.
try:
        while True:
			s.connect((host, port))
			print s.recv(1024)
   except KeyboardInterrupt:
    pass
 finally:
s.close                     # Close the socket when done